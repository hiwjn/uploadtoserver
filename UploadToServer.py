import paramiko
from scp import SCPClient
import sys, os, getopt
import json

def usage():
    print(
"""
------- UploadToServer.py Usage -------
--help: show help information
-h or --help: show help information
-c or --config: path of the config file (default path is current directory)   e.g. path/to/config.json
e.g. $ python3 UploadToServer.py -c /Users/username/config.json
or $ python3 UploadToServer.py
-----------------------------------
"""
    )

def log(information):
    print('[Info] ' + information)

def log_e(reason):
    print('[Error] ' + reason)

def log_ex(reason):
    log_e(reason)
    sys.exit()

def checkPathExists(path):
    if not os.path.exists(path):
        log_ex('path: \' ' + path + '\' not exists')

def createSSHClient(server, port, user, password):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, port, user, password)
    return client

def isDirOrFile(path):
    if os.path.isdir(path):  
        log('path: \'' + path + '\' is a directory') 
        return True
    elif os.path.isfile(path):  
        log('path: \'' + path + '\' is a normal file')   
        return False
    else:  
        log('path: \'' + path + '\' is a special file (socket, FIFO, device file)') 
        return False

def parseConfigFile(path):
    config_string = open(path).read()
    configs = ()
    try:
        config_json = json.loads(config_string)
        c_ip = config_json['server']['ip']
        c_port = config_json['server']['port']
        c_user = config_json['server']['user']
        c_password = config_json['server']['password']
        c_source = config_json['source']
        c_target = config_json['target']

        if c_ip == '' or c_port == '' or c_user == '' or c_password == '' or c_source == '' or c_target == '':
            log_ex('some infomation is empty in config.json')
        checkPathExists(c_source)
        configs = (c_ip, c_port, c_user, c_password, c_source, c_target)
    except e as e:
        print(e)
        log_ex('Parse har wrong, please check the har file')
    else:
        log('Parse config file done.')
        return configs

        
def uploadToServer(configs):
    ip, port, user, password, source, target = configs
    
    log('creating scp client...')
    ssh = createSSHClient(ip, port, user, password)
    scp = SCPClient(ssh.get_transport())

    log('uploading...')
    if isDirOrFile(source):
        scp.put(source, recursive=True, remote_path=target)
    else:
        scp.put(source, target)
    scp.close()
    log('upload done!')
        

def main(argv):
    inputConfigPath = ''
    try:
        opts, args = getopt.getopt(argv, "hc:", ["help", "config="])
    except getopt.GetoptError:
        log_ex('got error when parsing args')
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
            sys.exit()
        elif opt in ('-c', '--config'):
            checkPathExists(arg)
            inputConfigPath = arg
    if inputConfigPath == '':
        inputConfigPath = 'config.json'
        checkPathExists(inputConfigPath)
    configs = parseConfigFile(inputConfigPath)
    uploadToServer(configs)

if __name__ == "__main__":
    main(sys.argv[1:])