# UploadToServer

A script that can upload a file or a directory to your server. 

## Requirements

- Python 3
- paramiko

To install paramiko, just type

```
$ pip3 install paramiko
```

## Usage

First fill in the configuration file `config.json`.

```
{
    "server" : {
        "ip": "1.1.1.1",
        "port": "22",
        "user": "username",
        "password": "123456"
    },
    "source": "/Users/yourname/path/to/file/or/directory",
    "target": "/home/yourname/path/to/file/or/directory"
}
```

In the script root directory, type 

```
$ python3 UploadToServer.py -c paht/to/config.json
```

If the config.json is in the same directory with the script, you can just type

```
$ python3 UploadToServer.py
```

*Note: If there is already a file under the direcotry, the file will be overwrite. So be careful!*
 
## Examples

### Upload a file

In `config.json`, the `target` can be a direcotry or a file.

Upload a file to a file:

```
    "source": "/Users/yourname/Desktop/1.txt",
    "target": "/home/yourname/Documents/1.txt"
```

The target name can be anything you input:

```
    "source": "/Users/yourname/Desktop/1.txt",
    "target": "/home/yourname/Documents/xxx.txt"
```

Upload a file under a directory:

```
    "source": "/Users/yourname/Desktop/1.txt",
    "target": "/home/yourname/Documents"
```

### Upload a directory

In `config.json`, the `target` must be a directory and exist on the server. 

```
    "source": "/Users/yourname/Desktop/test",
    "target": "/home/yourname/Documents/test"
```

The target name can be anything you input:

```
    "source": "/Users/yourname/Desktop/test",
    "target": "/home/yourname/Documents/othername"
```

If you want to upload the directory under the `test`, add `/` at the end of the path. (the test path will be `/home/yourname/Documents/test/test`)

```
    "source": "/Users/yourname/Desktop/test",
    "target": "/home/yourname/Documents/test/"
```